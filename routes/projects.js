var express = require('express');

var config = require('../config.json');

var router = express.Router();

var uuid = require('node-uuid');


var multer  = require('multer');
var upload = multer({ dest: config.tmp_uploads_path, limits:{ fileSize: 100*1024*1024} });


var Promise = require('bluebird');
var fsPromise = Promise.promisifyAll(require("fs"));

var mkdirp = Promise.promisify(require('mkdirp'));

var path = require('path');

var db = require('../db.js');


router.get('/getViewURLForProjectName', function(req, res, next) {
  var name = req.query.name;
  var url =  req.protocol + '://'+name+'.'+req.get('host');
  res.status(200).send(url);
});

//create new
router.post('/new', function(req, res, next) {
  Promise.resolve()
  .then(function(){
    var password = uuid.v4();
    var given_name = req.body.desiredName.toLowerCase().replace(/[\W|_]+/g, "_")+Math.floor(Math.random()*100000);

    return db.Project.create({
      name: given_name,
      password: password
    })
  })
  .then(function(obj){
    res.status(200).send({
      name: obj.name,
      password: obj.password
    });
  })
  .catch(function(e){
    console.error(e.stack);
    res.status(500).send('Internal error');
  })

});

var findProjectWithNameAndPassword = function(name, password){
  return Promise.resolve()
  .then(function(){
    return db.Project.findAll({
      where: {
        name: name
      }
    });
  })
  .then(function(projects){
    if(projects.length == 0){
      return {error: 'project not found'};
    }else{
      var project = projects[0];
      if(project.password === password){
        return {project: project};
      }else{
        return {error: 'password is wrong'};
      }
    }
  })
};

var getPathForProjectName = function(name){
  return config.project_files_path+'/'+name+'/';
};

router.post('/add_file', upload.single('the_file'), function(req, res, next) {
  Promise.resolve()
  .then(function(){
    return findProjectWithNameAndPassword(req.body.name, req.body.password);
  })
  .then(function(find_results){
    if(find_results.project){
      if(req.file){
        console.log(req.file.path);
        if(req.body.dest.match(/\.\.\//)){
          throw new Error('path contains .. ');
        }
        var final_path = getPathForProjectName(find_results.project.name)+req.body.dest;
        return Promise.resolve()
        .then(function(){
          return mkdirp(path.dirname(final_path));
        })
        .then(function(){
          return fsPromise.rename(req.file.path, final_path);
        })
        .then(function(){
          res.status(200).send('Okay');
        })


      }else{
        res.status(500).send('No file upload given');
      }

    }else{
      res.status(404).send(find_results.error);
    }
  })
  .catch(function(e){
    console.error(e.stack);
    res.status(500).send('Internal error');
  })

});


router.post('/mkdir', upload.single('unused'), function(req, res, next) { // just to allow multipart
  Promise.resolve()
  .then(function(){
    return findProjectWithNameAndPassword(req.body.name, req.body.password);
  })
  .then(function(find_results){
    if(find_results.project){
      console.log(req.body.dest);
      if(req.body.dest.match(/\.\.\//)){
        throw new Error('path contains .. ');
      }
      var final_path = getPathForProjectName(find_results.project.name)+req.body.dest;
      return Promise.resolve()
      .then(function(){
        return mkdirp(final_path);
      })
      .then(function(){
        res.status(200).send('Okay');
      })
    }else{
      res.status(404).send(find_results.error);
    }
  })
  .catch(function(e){
    console.error(e.stack);
    res.status(500).send('Internal error');
  })

});

module.exports = router;
