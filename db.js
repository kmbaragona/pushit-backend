var Sequelize = require('sequelize');

var uuid = require('node-uuid');

var config = require('./config.json');

var user = config.db_user;
var pass = config.db_pass;



var sequelize = new Sequelize('pushit', user, pass, {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

sequelize
  .authenticate()
  .then(function(err) {
    console.log('Connection has been established successfully.');
  })
  .catch(function (err) {
    console.log('Unable to connect to the database:', err);
  });

var Project = sequelize.define('project', {
  name: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  },
},{});

Project.sync({force:false}).then(function () {
  // Table created
});

module.exports = {
  sequelize: sequelize,
  Project: Project
};
